TARGET=homework
LIVE=pdf

OUT_DIR=out
BUILD_DIR=build
TEX_DIR=tex
MONITOR=$(TEX_DIR)/$(TARGET)

all: $(patsubst $(TEX_DIR)/%.tex,$(OUT_DIR)/%.pdf,$(wildcard $(TEX_DIR)/*.tex))

$(OUT_DIR)/%.pdf: $(TEX_DIR)/%.tex
	mkdir -p "$(OUT_DIR)" "$(BUILD_DIR)"
	cp "$<" "$(BUILD_DIR)"
	cp ./res/* "$(BUILD_DIR)"
	pdflatex -interaction nonstopmode -output-directory "$(BUILD_DIR)" "$(BUILD_DIR)/$(<F)"
	cp "$(BUILD_DIR)/$(@F)" "$(OUT_DIR)/$(@F)"

clean:
	cd "$(OUT_DIR)" && rm -rfv ./*
	cd "$(BUILD_DIR)" && rm -rfv ./*

view: $(OUT_DIR)/$(TARGET).pdf
	xdg-open "$(OUT_DIR)/$(TARGET).pdf" >/dev/null 2>&1

live: $(MONITOR).tex
	./scripts/live-prev.sh "$(OUT_DIR)" "$<" "$(LIVE)" || true
	

.PHONY: all clean view live
