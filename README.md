# LaTeX Template

This is a template to clone for each LaTeX project you work on.  You can convert it, view it, and live preview it.

*Note:* Much of this was taken from **cmugpi**'s [repository on GitHub](https://github.com/cmugpi/latex-sample) on 2021.05.04 following its MIT licensing.

*Note:* Although this is working, it is still a work in progress and largely in development.  If you have any bugs or problems, please submit an issue to [GitLab](https://gitlab.com/NicholasHein/latex-template).

## Structure

```
/
|
|`-build
|`-node_modules
|`out
| |`live
|  `homework.pdf
|`res
|  `-homework.cls
|`scripts
|`srv
|`tex
|  `-homework.tex
|`Makefile
|`package.json
|`package-lock.json
 `README.md
```

  - `build`: All the intermediate generated files
  - `node_modules`: General code to support the live preview
  - `out`: Where the output goes
    - `live`: General space to host the live output
    - `homework.pdf`: An example PDF generated from the LaTeX
  - `res`: Resources for the LaTeX file
    - `homework.cls`: An example resource
  - `scripts`: Supporting shell scripts
  - `srv`: Files used for live preview
  - `tex`: Where your LaTeX files go
    - `homework.tex`: An example LaTeX file
  - `Makefile`: The code run by `make`
  - `package.json`
  - `package-lock.json`
  - `README.md`: This file

## Building

To generate your PDF output with `pdflatex`, run one of these commands.

```bash
$ make
```

This command takes all `.tex` files in your `/tex` directory, converts them to PDFs, and places the PDFs in the `/out` directory.

```bash
$ make clean
```

Cleans up generated files.  It deletes everything in `/out` and `/build`.

## Viewing

To view your output, you can access the files directly or use one of the commands below.

```bash
$ make view [TARGET=$YOUR_TEX_NAME]
```

This command generates your target PDF like `make` if needed, and it opens it in your default PDF viewer.  If no target is specified, the default is *"homework"*.

```bash
$ make live [TARGET=$YOUR_TEX_NAME] [LIVE=$DESIRED_EXTENSION]
```

This command generates your target output and opens it in your default browser somewhat like `make view`.  However, this command continues to run and reloads the browser page when you save your original LaTeX file to give a "live preview" feel.  The `LIVE` parameter specifies the type of preview you want.  The default is `pdf`, but it can be set to either `pdf` or `html`.
