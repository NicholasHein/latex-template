#!/bin/bash

set -e

trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT

IN_FILE=$2
OUT_DIR=$1
EXT=$3

if [ -z $EXT ]; then
	EXT='html'
fi


# Switch to root of project
MY_PATH=$(dirname "${BASH_SOURCE[0]}")
cd "$MY_PATH/.."

function build_output() {
	# Prepare live directory
	LIVE_DIR="$OUT_DIR/live"
	mkdir -p "$LIVE_DIR"


	# Populate live directory with make4ht (using an intermediate temporary directory)
	IN_BASE="$(basename $IN_FILE)"
	IN_NAME="${IN_BASE%.*}"
	TMP_DIR="./build"
	mkdir -p "$TMP_DIR"
	cp -f "$IN_FILE" "$TMP_DIR"
	cp -f ./res/* "$TMP_DIR"
	RET_DIR="$(pwd)"
	cd "$TMP_DIR"
	if [ $EXT == 'pdf' ]; then
		pdflatex -interaction nonstopmode "$IN_BASE"
	else
		make4ht "$IN_BASE" 'mathml'
	fi
	cd "$RET_DIR"
	mv "$TMP_DIR"/* "$LIVE_DIR"

	# Set up HTML environment
	if [ $EXT == 'pdf' ]; then
		mv "$LIVE_DIR/$IN_NAME.pdf" "$LIVE_DIR/inner.pdf"
		cp -f "srv/index.pdf.html" "$LIVE_DIR/index.html"
	else
		mv "$LIVE_DIR/$IN_NAME.html" "$LIVE_DIR/inner.html"
		cp -f "srv/index.html.html" "$LIVE_DIR/index.html"
	fi
}
build_output

# Start up livereload server
./node_modules/livereload/bin/livereload.js -u true "$LIVE_DIR" &

# Open file with default app
xdg-open "$LIVE_DIR/index.html" >/dev/null 2>&1 &


inotifywait -r -m -e modify "$(dirname $IN_FILE)" | \
	while read path _ file; do \
		echo "Compiling: '$IN_FILE'"; \
		build_output; \
	done
